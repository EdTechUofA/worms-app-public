var PhotoView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        //this.render();
        this.wormData = wormData[wormData.length - 1];
        // console.log('photoview');
		self = this;
    };
    
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};
	
	this.captureData = function () {
		this.wormData.notes = $('textarea').val();

	}
	
	this.setDefaults = function () {
		this.setupEvents();
		$('textarea').val(this.wormData.notes);		
		if (this.wormData.photo.length > 0 ) {
			self.displayPhotoSize(this.wormData.photo);
		    var $img = $("<img class='img-responsive'>");
		    $img.attr('src', this.wormData.photo);
		    $('.photobox').html($img);
		}
		
		if (!navigator.camera) {
			$('#takephoto').removeClass('btn-success');
			$('#takephoto').addClass('btn-danger');
		}
	}

	this.setupEvents = function () {
		// capture all data on screen if either nav button is tapped
		$('a').slice(0,2).on('click',function (){
			self.captureData();
		});
		
		$('#takephoto').on('click', function () {
			self.takePhoto();
		});
	}
	
	this.wormsButtonClicked = function ($button) {
		$('.wormsButton').removeClass('active');
		$button.addClass('active');
	}
	
	this.displayPhotoSize = function (photo) {
		  // find the photo's size and display it above the photo thumbnail
		  window.resolveLocalFileSystemURL(photo, function(fileEntry) { 
			  fileEntry.file(function(file) {
				  var imageSize = Math.round(file.size / 1024);
				  $('#photoSize').text('Image size: ' + imageSize + ' KB');
			  });
		
		  }, function(){ 
				// do nothing if it fails
		  });
	}
		
	this.takePhoto = function () {		
		if (!navigator.camera) {
			var $modal = $('#helpModal');
			$modal.find('.modal-title').text("Photo Error");
			$modal.find('.modal-body').html('The photo feature is not available on web browsers. You must use the mobile app if you want to submit a photo.');
			$modal.modal('show');
			return;
		}
		var options =   {   quality: 50,
							destinationType: 1,  //DATA_URL : 0, FILE_URI : 1, NATIVE_URI : 2
							sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
							encodingType: 0,     // 0=JPG 1=PNG
							correctOrientation: true
						};

		navigator.camera.getPicture(
			  // success
			  function(photo) {
			  	  self.wormData.photo = photo;
			  	  // insert a thumbnail of the photo and display its size
				  self.displayPhotoSize(photo);
				  var $img = $("<img class='img-responsive'>");
				  $img.attr('src', photo);
				  $('.photobox').html($img);
			  },
			  
			  // failure
			  function() {
// 				var $modal = $('#helpModal');
// 				$modal.find('.modal-title').text("Photo Error");
// 				$modal.find('.modal-body').html('Your mobile device is unable to take a photo at this time.');
// 				$modal.modal('show');
			  },
			  
			  options);

		  return false;
	}
	
    this.initialize();

}