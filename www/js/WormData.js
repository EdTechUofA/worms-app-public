function Sighting() {
					// app version should be entered here for each revision
					this.appVersion = '2.0';

					this.locationStarted = false;
					this.locationModalPermitted = true;
    				this.latitude = "";
    				this.longitude = "";
    				this.gpsoverride = false;
					this.desc = "";
					this.litter = "";
					this.impactbuildings = false;
					this.impactcrops = false;
					this.impactfishing = false;
					this.impactgrazing = false;
					this.impactpaths = false;
					this.impactroads = false;
					this.impactterrain = false;
					
					this.samplingStarted = false;
					this.samplingModalPermitted = true;
					this.date = "";
					this.weather = "";
					this.moisture = "";
					this.samplingMethod = "";
					this.middenCount = "";
					this.standardPlot = "";
					this.wormsFound = "";
					
					this.detailsStarted = false;
					this.detailsModalPermitted = true;
					this.showDetailsAboutModal = true;
					this.count = "";
					this.wormType = "";
					this.wormLength = "";
					this.colour = "";
		            this.gradient = false;
		            this.clitellum = false;
		            this.diameterGT2 = false;
		            this.tailFlattens = false;
					
					this.notes = "";
					this.photo = "";
					
					this.submitStarted = false;
					this.submitModalPermitted = true;
					this.userType = "";
					this.age = null;
					this.groupID = "";


// 					this.locationStarted = true;
// 					this.locationModalPermitted = true;
//     				this.latitude = "22.123456";
//     				this.longitude = "-155.123456";
//     				this.gpsoverride = false;
// 					this.desc = "Grassland";
// 					this.litter = "2";
// 					this.impactbuildings = true;
// 					this.impactcrops = true;
// 					this.impactfishing = true;
// 					this.impactgrazing = true;
// 					this.impactpaths = false;
// 					this.impactroads = false;
// 					this.impactterrain = true;
// 					
// 					this.samplingStarted = true;
// 					this.samplingModalPermitted = true;
// 					this.date = "2015-03-01";
// 					this.weather = "foo";
// 					this.moisture = "Dry";
// 					this.samplingMethod = "Flip and Strip";
// 					this.middenCount = "2";
// 					this.standardPlot = "Yes";
// 					this.wormsFound = "Yes";
// 					
// 					this.detailsStarted = true;
// 					this.detailsModalPermitted = true;
// 					this.showDetailsAboutModal = true;
// 					this.count = "4";
// 					this.wormType = "Juvenile";
// 					this.wormLength = "7 cm - 12 cm";
// 					this.colour = "White";
// 		            this.gradient = true;
// 		            this.clitellum = true;
// 		            this.diameterGT2 = true;
// 		            this.tailFlattens = true;
// 					
// 					this.notes = "foo";
// 					this.photo = "";
// 					
// 					this.submitStarted = true;
// 					this.submitModalPermitted = true;
// 					this.userType = "Junior High School Student";
// 					this.age = "9";
// 					this.groupID = "abc";
					}

