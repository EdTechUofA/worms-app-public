var DetailsView = function (wormData) {

	this.initialize = function () {
        // Define a div wrapper for the view (used to attach events)
        this.$el = $('<div class="templatehtml"/>');
        //this.render();
        this.wormData = wormData[wormData.length - 1];
        // console.log('detailsview: ');
		self = this;
    };
    
    this.render = function() {
        this.$el.html(this.template());
        return this;
	};
	
	this.captureData = function () {
		this.wormData.count = $('#count').val();
		this.wormData.wormType = $('#wormType').val();
		this.wormData.wormLength = $('#wormLength').val();
		this.wormData.colour = $('#colour').val();
		this.wormData.gradient = $('#gradient').prop('checked');
		this.wormData.clitellum = $('#clitellum').prop('checked');
		this.wormData.diameterGT2 = $('#diameterGT2').prop('checked');
		this.wormData.tailFlattens = $('#tailFlattens').prop('checked');
	}
	
	this.setDefaults = function () {
		this.setupEvents();
		$('#count').val(this.wormData.count);
		$('#wormType').val(this.wormData.wormType);
		$('#wormLength').val(this.wormData.wormLength);
		$('#colour').val(this.wormData.colour);
		$('#gradient').prop('checked', this.wormData.gradient);
		$('#clitellum').prop('checked', this.wormData.clitellum);
		$('#diameterGT2').prop('checked', this.wormData.diameterGT2);
		$('#tailFlattens').prop('checked', this.wormData.tailFlattens);
		
		// highlight required inputs with bad data
		if (this.wormData.detailsStarted) {
			this.testClitellum();

			if ($('#count').val().length == 0) {
				$('#count').parent().addClass('has-error');
			};
			if ($('#wormType').val().length == 0) {
				$('#wormType').parent().addClass('has-error');
			};
			if ($('#wormLength').val().length == 0) {
				$('#wormLength').parent().addClass('has-error');
			};
			if ($('#colour').val().length == 0) {
				$('#colour').parent().addClass('has-error');
			};
		}
		
		// put up warning modal if there are still errors on the page
		if ($('div.has-error').length > 0 && self.wormData.detailsModalPermitted) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("Errors Found");
		  	$modal.find('.modal-body').html('There are errors in the <strong>required</strong> fields which are identified by an asterisk.<br>The affected fields are highlighted in <span style="color:red">red</span>.');
		  	$modal.modal('show');
		  	// turn off modal error dialog if its been displayed once already
			self.wormData.detailsModalPermitted = false;
		}

		if (self.wormData.showDetailsAboutModal) {
			var $modal = $('#helpModal');
		  	$modal.find('.modal-title').text("About Details");
		  	$modal.find('.modal-body').html('<p>On this screen you will enter data for each type of worm that you find. Before entering data, group your worms by type. If you are not sure of the types of worms, click on the info button next to “Worm Type” for more information.</p>After recording data for the first type of worm and submitting a picture (optional) you will be asked if you want to enter data for “More Worms” (at this location) or if you want to enter data for a “New Location”.');
		  	$modal.modal('show');
		  	self.wormData.showDetailsAboutModal = false;
		}

	}

	this.setupEvents = function () {
		// modal help handler
		$('#helpModal').on('show.bs.modal', function (event) {
		  var $button = $(event.relatedTarget); // Button that triggered the modal
		  var helpTitle = $button.data('title'); // Extract info from data-* attributes
		  var tmplSelector = $button.data('html');
		  var helpText = $('#modalTemplates').find(tmplSelector).html();
		  var $this = $(this);
		  $this.find('.modal-title').text(helpTitle);
		  $this.find('.modal-body').html(helpText);
		});
		
		$('#count').on('change', self.testCount);
		$('#wormType').on('change', self.testWormType);
		$('#wormLength').on('change', self.testWormLength);
		$('#colour').on('change', self.testColour);
		$('#clitellum').on('click', self.testClitellum);
		
		// capture all data on screen if either nav button is tapped
		$('a').slice(0,2).on('click',function (){
			self.captureData();
		});

	}
	
	this.testCount = function () {
		self.wormData.detailsStarted = true;
		var $count = $('#count');
		var val = $count.val();
		var countParsed = parseInt(val,10);
		if (countParsed > 0 ) {
			$count.parent().removeClass('has-error');
			$count.val(countParsed);
		} else {
			$count.parent().addClass('has-error');
			$count.val("");
		}
	}
	
	this.testWormType = function () {
		self.wormData.detailsStarted = true;
		var $wormType = $('#wormType');
		if ($wormType.val().length == 0) {
			$wormType.parent().addClass('has-error');
		} else {
			$wormType.parent().removeClass('has-error');
		}
	}
	
	this.testWormLength = function () {
		self.wormData.detailsStarted = true;
		var $wormLength = $('#wormLength');
		if ($wormLength.val().length == 0) {
			$wormLength.parent().addClass('has-error');
		} else {
			$wormLength.parent().removeClass('has-error');
		}
	}
	
	this.testColour = function () {
		self.wormData.detailsStarted = true;
		var $colour = $('#colour');
		if ($colour.val().length == 0) {
			$colour.parent().addClass('has-error');
		} else {
			$colour.parent().removeClass('has-error');
		}
	}
	
	this.testClitellum = function () {
		self.wormData.detailsStarted = true;
		if ($('#clitellum').prop('checked')) {
			$('#diameterGT2').prop('disabled',false);
			$('#diameterGT2').closest("div").removeClass('disabled');
			$('#tailFlattens').prop('disabled',false);
			$('#tailFlattens').closest("div").removeClass('disabled');
			
		} else {
			$('#diameterGT2').prop('disabled',true);
			$('#diameterGT2').prop('checked',false);
			$('#diameterGT2').closest("div").addClass('disabled');
			$('#tailFlattens').prop('disabled',true);
			$('#tailFlattens').prop('checked',false);
			$('#tailFlattens').closest("div").addClass('disabled');
		}
	}

    this.initialize();

}